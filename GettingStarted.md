# Getting Started
How to use HarmonyOS Hackathon resources

## Resources
The goal of the hackathon is to create inovative home automation scenarios with devices running Oniro.    
To support participants we provide several demo projects and recipes to integrate the projects into Oniro device images.     
We can group the resources by demo projects, meta-layers and integration support.   

### Meta layers and integration
1. Local Manifests - The local manifests file to be added to repo workspace   
https://gitlab.com/harmonyos-hackathon/resources-oniro/local-manifests
2. Meta DPE High - The Yocto meta layer that provides recipes targeting Oniro Linux flavor
https://gitlab.com/harmonyos-hackathon/resources-oniro/meta-dpe-high
3. Meta DPE Low - The Yocto meta layer that provides recipes targeting Oniro Zephyr flavor
https://gitlab.com/harmonyos-hackathon/resources-oniro/meta-dpe-low

### Demo projects
1. Smart Lamp Demo - A simple demo application that registers for lamp state events on DBus from managers and set the GPIO controling the lamp switch
https://gitlab.com/harmonyos-hackathon/resources-oniro/smart-lamp-demo
2. MQTT Manager - Example of MQTT manager that register, publish and subscribe to a broker a topic defining a smart lamp switch
https://gitlab.com/harmonyos-hackathon/resources-oniro/mqtt-manager  
3. NRF52830dk Demo - A simple Zephyr flavor demo application that presents how drivers and modules can be added
https://gitlab.com/harmonyos-hackathon/resources-oniro/nrf52840dk-demo  

## Building the demos
Oniro is able to build images for different type of devices from the same source tree by setting the flavor build environment.   
1. Get access to Harmony OS Hackathon resources group and resources projects
* Request access to resources if not available for your GitLab account
* Generate an access token to use in your build machine
* Add gitlab.com machine with the access token in your ~/.netrc file
```
machine gitlab.com
  login <username>
  password <access_token>
```

2. Setup build environment following the steps in Local Manifests readme file: https://gitlab.com/harmonyos-hackathon/resources/local-manifests/-/blob/main/README.md
3. Setup workspace for Oniro    
```
# repo init -u https://booting.oniroproject.org/distro/oniro
# repo sync --no-clone-bundle
```
4. Clone local manifests repository as repo local manifests
```
# git clone git@gitlab.com:harmonyos-hackathon/resources-oniro/local-manifests.git .repo/local_manifests -b main
# repo sync --no-clone-bundle
```
5. Follow meta layer specific instructions      
This will provide instructions how to build an image with one of the meta-layers using the workspace just created:    
* High Devices: https://gitlab.com/harmonyos-hackathon/resources-oniro/meta-dpe-high
* Low Devices: https://gitlab.com/harmonyos-hackathon/resources-oniro/meta-dpe-low

6. Get more information about the demo      
https://gitlab.com/harmonyos-hackathon/resources-oniro/documentation/-/blob/master/SmartLampDemo.md   

## Automation environment
To demonstrate the automation capabilities of smart home scenarios the participants will create, we propose to open source automation platforms and services.   

### Platforms
Any open source automation platform can be used, this includes (not limiting) one of:   
* Home Assistant - https://www.home-assistant.io/
* OpenHAB - https://www.openhab.org/
* Node-RED - https://nodered.org/

### Services
Additional open source servers can be used as long as the configuration is shared as part of submissions.    
The Smart Lamp Demo provided with Hackathon resources is using MQTT as an example for IPC with the automation platform.    
* Mosquitto - https://mosquitto.org 

