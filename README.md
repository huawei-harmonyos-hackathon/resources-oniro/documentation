# Welcome to HarmonyOS Hackathon
Let's play with HarmonyOS and craft the next gen IoT devices and user scenarios for Smart Home

## Content
1. Getting Started - https://gitlab.com/harmonyos-hackathon/resources-oniro/documentation/-/blob/master/GettingStarted.md
1. Smart Lamp Demo - https://gitlab.com/harmonyos-hackathon/resources-oniro/documentation/-/blob/master/SmartLampDemo.md
