# Smart Lamp Demo
A simple smart lamp demo application and Home Assistant integration    

## Description
To demonstrate the use of Oniro and component integration a simple smart lamp demo is provided with Hackathon resources.   
This demo is using Oniro Linux flavor and does the simple task of switching on and off one LED via GPIO pin.    
The demo makes use of two components, Smart Lamp Demo application and the MQTT Manager that interact via DBus on target device.    

## Dependencies
Please make sure the hardware and software dependencies are available    

### Hardware
* One Raspberry Pi4 board including accesories - https://www.raspberrypi.org/products/raspberry-pi-4-model-b/
* USB to UART adaptor connected to UART pins on RPi4 (for debug)
* One LED with wires connected on GPIO21. This is a 5v pin and will work with with an 5v LED without additional resistor.

### Software
* Access to Hackthon resources on gitlab.com
* Linux build environment

## Replicate Smart Lamp Demo
To replicate the Smart Lamp Demo we need to build oniro-image-base with smartlamp-demo and mqtt-manager as extra packages.    

### Device
Build and configure the Linux image for RPi4:
1. Setup Linux flavor workspace - See https://gitlab.com/harmonyos-hackathon/resources-oniro/documentation/-/blob/master/GettingStarted.md
2. Install bmaptool in your host machine
```
# sudo apt-get install bmap-tools
```
3. Add nm-rw-helper, smartlamp-demo and mqtt-manager to IMAGE_INSTALL list in your conf/local.conf file
```
IMAGE_INSTALL_append = " nm-rw-helper mqtt-manager smartlamp-demo"
```
4. Build the image
```
# MACHINE=raspberrypi4-64 bitbake oniro-image-base
```
5. Flash the image to SD card
```
# sudo bmaptool copy oniro-image-base-raspberrypi4-64.wic.bz2 /dev/<sd_card_node>
```
6. Copy configuration files to appdata partition
The Smart Lamp Demo and MQTT Manager will run as systemd services and will read the configuration from appdata partition.
By default the image doesn't include this provisions so this needs to be copied post build and changed (eg. GPIO, MQTT broker address, etc).
```
# cp -rf <path_to_mqtt_manager_repo>/tests/provisions/MQTT <appdata_mount_point>/
# cp -rf <path_to_smartlamp_demo_repo>/tests/provisions/SmartLamp <appdata_mount_point>/
```
7. Unmount SD card from host, insert into RaspberryPi4 device and boot
8. Connect the device on you local network
The image comes with NetworkManager service installed and can be used to bring the device online using WiFi
```
# nmcli dev wifi connect "<network_ssid>" password "<wifi_password>"
```
    
### MQTT broker
To connect the device with Home Assistant the MQTT broker server should be availble.    
Any public MQTT broker can be used or participants can setup a mosquitto broker on local network.     
Make sure that the device is using this broker address in appdata/MQTT/mqttmanager.conf    

### Home Assistant 
An instance of home assistant needs to be setup on the local machine or in the cloud.    
The home assistant provides MQTT integration: https://www.home-assistant.io/docs/mqtt/discovery/    
Once the Home Assistant is running the device can be added as an MQTT switch device.    
Please note that the topic used by mqtt-manager is "/huawei/smartlamp/switch".    
For more information please check the mqm-adapter module:   
https://gitlab.com/harmonyos-hackathon/resources-oniro/mqtt-manager/-/blob/main/source/mqm-adapter.c
